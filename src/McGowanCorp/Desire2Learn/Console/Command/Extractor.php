<?php
/**
 * McGowan Corp. PHP Library
 *
 * Copyright (c) 2012-2013, McGowan Corp. (Aaron McGowan) <aaron@mcgowancorp.com>.
 * All rights reserved.
 *  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *  
 *   * Neither the name or logo of McGowan Corp., or Aaron McGowan, nor
 *     the names of their contributors may be used to endorse or promote
 *     products derived from this software without specific prior written
 *     permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

namespace McGowanCorp\Desire2Learn\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use McGowanCorp\Desire2Learn\DropBox as DropBox;

/**
 * A single Symfony command for extracting Desire 2 Learn's DropBox submission
 * download zip archive, specifying the destination. Has the capabilities of performing
 * a single step recursive extract on the source archive's contents.
 * 
 * @package McGowanCorp\Desire2Learn\Console\Command
 * @version 0.0.1
 */
class Extractor extends Command
{
    protected function configure()
    {
        $this->setName('d2l:extractor')
            ->setDescription('Extracts all student submissions for a single DropBox archive of submissions recursively.')
            ->setDefinition(array(
                new InputArgument('source', InputArgument::REQUIRED, 'The source archive of a single DropBox\'s submissions.'),
                new InputArgument('destination', InputArgument::REQUIRED, 'The destination directory in which all items within the archive should be extracted to.'),
                new InputOption('recursive', 'r', InputOption::VALUE_NONE, 'Perform a one-step recursive call on extracted contents of the source archive.')
            ));
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $extractor = new DropBox\Extractor;
        
        try
        {
            $extractor->setSourceArchive($input->getArgument('source'))
                ->setDestination($input->getArgument('destination'))
                ->extract((bool) $input->getOption('recursive'));
            $output->writeln(sprintf('Successful unpackaging of %s.', $extractor->getSourceArchive()->getFilename()));
        }
        catch( \Exception $exception )
        {
            $output->writeln(sprintf('%s', $exception->getMessage()));
        }
    }
}