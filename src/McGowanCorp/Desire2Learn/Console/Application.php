<?php
/**
 *
 *
 */

namespace McGowanCorp\Desire2Learn\Console;

use Symfony\Component\Console\Application as ConsoleApp;
use Symfony\Component\Console\Input\InputInterface;

use McGowanCorp\Desire2Learn\Console\Command\Extractor;

/**
 *
 *
 */
final class Application extends ConsoleApp
{
    protected function getDefaultCommands()
    {
        $default = parent::getDefaultCommands();
        $default[] = new Extractor;
        
        return $default;
    }
}