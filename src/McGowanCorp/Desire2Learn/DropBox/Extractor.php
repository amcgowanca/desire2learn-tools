<?php
/**
 * McGowan Corp. PHP Library
 *
 * Copyright (c) 2012-2013, McGowan Corp. (Aaron McGowan) <aaron@mcgowancorp.com>.
 * All rights reserved.
 *  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *  
 *   * Neither the name or logo of McGowan Corp., or Aaron McGowan, nor
 *     the names of their contributors may be used to endorse or promote
 *     products derived from this software without specific prior written
 *     permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */
namespace McGowanCorp\Desire2Learn\DropBox;

/**
 * Simple class used for handling the extracting of Desire 2 Learn's Course DropBox submissions
 * when downloaded as an entire ZIP archive. 
 *
 * Currently Extractor only supports Zip archives. When extracting will perform a one-step recursive extract
 * allowing student submissions to be extracted from the source archive to a destination directory
 * in the following structure.
 *
 * Source archive: "DropBox-Name-Here Date-of-Download.zip"
 *  -> (contents)
 *      -> "last-name first-name date-of-submission - file-name.zip"
 *
 * Extracted contents:
 * 
 * /destination-path/
 *   -> /first-name last-name/
 *      -> /date-of-submission/
 *         -> Extracted contents located here.
 *      -> "last-name first-name date-of-submission - file-name.zip"
 *
 * @package McGowanCorp\Desire2Learn\DropBox
 * @author Aaron McGowan <aaron.mcgowan@mcgowancorp.com>
 * @version 0.0.1
 */
class Extractor
{
    const SUBMISSION_ARCHIVE_REGEX = '([a-zA-Z-]+)([\\s]+)([a-zA-Z-]+)([\\s]+)(([a-zA-Z]{3})([\\s]+)([0-9]{1,2})([,])([\\s]+)([0-9]{4})([\\s]+)([0-9]+)([\\s]+)(AM|PM))';
    
    /**
     * @var string An absolute path to the destination archive.
     */
    private $destination_raw;
    
    /**
     * @var \SplFileInfo A SplFileInfo object used to represent the destination directory.
     */
    private $destination = null;
    
    /**
     * @var string An absolute path to the source archive.
     */
    private $source_raw;
    
    /**
     * @var \SplFileInfo A SplFileInfo object used to represent the source archive.
     */
    private $source = null;
    
    /**
     * @var bool Used to determine if this source and destination has been extracted.
     */
    private $has_extracted = false;
    
    /**
     * @var array An array of files to be ignored in a recursive extract.
     */
    private $ignored_files = array('.', '..', 'index.html', '.DS_Store');
    
    /**
     * Creates a new instance of the Extractor object.
     *
     * @access public
     * @param string $source The file path to the source archive.
     * @param string $destination The directory path to the destination.
     */
    public function __construct($source = null, $destination = null)
    {
        $this->setSourceArchive($source);
        $this->setDestination($destination);
    }
    
    /**
     * Returns the destination directory as an SplFileInfo object.
     *
     * @access public
     * @return \SplFileInfo The destination directory.
     */
    public function getDestination()
    {
        return $this->destination;
    }
    
    /**
     * Sets the path of the destination directory in which the source archive
     * is to be extracted (decompressed) to.
     *
     * @access public
     * @param string $destination The path to the destination directory.
     * @return Extractor Returns this instance of the extractor on success otherwise false.
     * @throws \InvalidArgumentException Thrown if the destination is not a valid directory and/or is not writable.
     */
    public function setDestination($destination)
    {
        $_destination = realpath($destination);
        if( !is_dir($_destination) || !is_writable($_destination) )
        {
            throw new \InvalidArgumentException(sprintf('The specified destination is invalid. The directory may not exist and/or it may not be writeable. [ Destination %s ]', $destination));
        }
        
        $this->destination_raw = $_destination;
        $this->destination = new \SplFileInfo($this->destination_raw);
        $this->has_extracted = false;
        
        return $this;
    }
    
    /**
     * Returns the source archive as an SplFileInfo object
     *
     * @access public
     * @return \SplFileInfo The source archive.
     */
    public function getSourceArchive()
    {
        return $this->source;
    }
    
    /**
     * Sets the source archive in which its contents should be extracted to the destination.
     *
     * @access public
     * @param string $source The path to the source archive file.
     * @return Extractor Returns this instance of the extractor on success otherwise false.
     * @throws \InvalidArgumentException Thrown if the source is not a valid file and/or is not readable.
     */
    public function setSourceArchive($source)
    {
        $_source = realpath($source);
        if( !file_exists($_source) || !is_readable($_source) )
        {
            throw new \InvalidArgumentException(sprintf('The specified source is invalid. The file may not exist or it may not be readable. [ Source: %s ]', $source));
        }
        
        $this->source_raw = $_source;
        $this->source = new \SplFileInfo($_source);
        $this->has_extracted = false;
        return $this;
    }
    
    /**
     * Performs the extract.
     *
     * @access public
     * @param bool $recursive Used to determine if a recursive extract should occur on its contents, one step only.
     * @throws \RuntimeException Thrown if the source or destination is not set or if the source is not of type Zip.
     */
    public function extract($recursive = false)
    {
        if( $this->has_extracted )
        {
            return;
        }
        
        if( !$this->source_raw || !$this->destination_raw )
        {
            throw new \RuntimeException(sprintf('The source or destination may not be set. [ Source: %s ] [ Destination: %s ]', $this->source_raw, $this->destination_raw));
        }
        
        if( 'zip' === $this->source->getExtension() )
        {
            $this->prepare($this->source, $this->destination, $recursive);
            $this->extractZipArchive($this->source, $this->destination, $recursive);
        }
        else
        {
            throw new \RuntimeException(sprintf('The source archive must be of type Zip. Ensure that this is a .zip archive verse another compressed file type.'));
        }
    }
    
    /**
     * Do preparation prior to extracting. Runs only if the archive
     * is in good shape and is ready to be extracted.
     * 
     * @access protected
     * @param \SplFileInfo $source The source archive.
     * @param \SplFileInfo $destination The destination directory.
     * @param bool $recursive Whether or not the archive should have a recursive extraction (one step).
     */
    protected function prepare(\SplFileInfo $source, \SplFileInfo $destination, $recursive)
    {
        /* void */
    }
    
    /**
     * Performs the operations required for extracting .zip archives.
     *
     * @access protected
     * @param \SplFileInfo $source The source archive.
     * @param \SplFileInfo $destination The destination directory of the archive.
     * @param bool $recursive Whether a recursive extract should be performed.
     * @return bool Returns true if the extract appears to be completed successfully, else false.
     */
    protected function extractZipArchive(\SplFileInfo $source, \SplFileInfo $destination, $recursive = false)
    {
        $zip = new \ZipArchive;
        if( true === $zip->open($source->getRealPath()) )
        {
            $zip->extractTo($destination->getRealPath());
            $zip->close();
            
            if( $recursive )
            {
                $this->doExtractZipArchiveRecursive($source, $destination);
            }
            
            $this->movefile($source, $destination . \DIRECTORY_SEPARATOR . $source->getFilename());
            return true;
        }
        
        return false;
    }
    
    /**
     * Handles the one step recursive extract of Zip archive sources.
     * 
     * @access protected
     * @param \SplFileInfo $source The source archive file.
     * @param \SplFileInfo $destination The destination directory of the archive.
     */
    protected function doExtractZipArchiveRecursive(\SplFileInfo $source, \SplFileInfo $destination)
    {
        $directory_iter = new \DirectoryIterator($destination->getRealPath());
        foreach( $directory_iter as $file )
        {
            if( !$file->isFile() )
            {
                continue;
            }
            
            if( in_array($file->getFilename(), $this->ignored_files) )
            {
                continue;
            }
            
            if( !preg_match('/^' . self::SUBMISSION_ARCHIVE_REGEX . '/', $file->getFilename(), $matches) )
            {
                continue;
            }
            
            if( false === ($student_dir = $this->makedir($destination . \DIRECTORY_SEPARATOR . ($matches[3] . ' ' . $matches[1]))) )
            {
                continue;
            }
            
            if( false === ($submission_dir = $this->makedir($student_dir . str_replace(array(','), array(''), $matches[5]))) )
            {
                continue;
            }
                
            $newname = $this->movefile($file->getRealPath(), $submission_dir . $file->getFilename());
            
            try
            {
                $extractor = new self($newname, $submission_dir);
                if( !$extractor->extract(false) )
                {
                    // TODO: Add logger functionality
                }
                unset($extractor);
            }
            catch( \Exception $exception )
            {
                /* Re-throwing the exception here should not be done. */
                // TODO: Add logger functionality
            }
        }
    }
    
    /**
     * Handles the creation of creating directories.
     * 
     * @access protected
     * @param string $directory The directory in question to create.
     * @param int $mode The mode to give to the directory.
     * @return string|bool The newly created directory path if created, otherwise false.
     * @uses mkdir http://php.net/mkdir
     */
    protected function makedir($directory, $mode = 0755)
    {
        if( !file_exists($directory) && !mkdir($directory, $mode) )
        {
            return false;
        }
        
        return rtrim($directory, \DIRECTORY_SEPARATOR) . \DIRECTORY_SEPARATOR;
    }
    
    /**
     * Moves a file from the original location to the new specified location.
     * 
     * @access protected
     * @param string $original The absolute path (including filename) for the original file.
     * @param string $new The absolute path (including filename) for the new file.
     * @return bool Returns the new
     * @uses rename http://php.net/rename
     */
    protected function movefile($original, $new)
    {
        return @rename($original, $new) ? $new : false;
    }
}