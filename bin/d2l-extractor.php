#!/usr/bin/env php
<?php

define('ROOT_PATH', realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR);
define('SOURCE_PATH', ROOT_PATH . 'src' . DIRECTORY_SEPARATOR);
define('VENDOR_PATH', ROOT_PATH . 'vendor' . DIRECTORY_SEPARATOR);

$autoloader = require_once VENDOR_PATH . 'autoload.php';
$autoloader->add('McGowanCorp', SOURCE_PATH);

use McGowanCorp\Desire2Learn\Console\Application;

$application = new Application('Desire 2 Learn Tools', '0.0.1');
$application->run();