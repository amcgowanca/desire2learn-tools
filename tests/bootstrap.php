<?php

define('D2LTOOLS_TESTS', true);

if( !defined('DS') )
{
    define('DS', DIRECTORY_SEPARATOR);
}

define('D2LTOOLS_ROOT_PATH', realpath(__DIR__ . DS . '..' . DS) . DS);