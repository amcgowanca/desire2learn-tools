<?php

namespace McGowanCorp\Desire2Learn\DropBox\Extractor\Tests;

/**
 * @ignore
 */
defined('D2LTOOLS_TESTS') or exit;

use McGowanCorp\Desire2Learn\DropBox\Extractor;

/**
 *
 *
 */
class ExtractorTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        require_once D2LTOOLS_ROOT_PATH . 'src/McGowanCorp/Desire2Learn/DropBox/Extractor.php';
    }
    
    public function testConstructorNoExceptions()
    {
        try
        {
            $extractor = new Extractor;
            $this->assertNotNull($extractor);
        }
        catch( \Exception $ex )
        {
            $this->fail('An unexpected exception was thrown.');
        }
    }
}